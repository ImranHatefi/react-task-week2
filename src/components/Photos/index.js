import React from 'react';
import PropTypes from 'prop-types';
import styles from './Photos.module.css';
const Photos = ({ photos, children }) => {
  return (
    <div className={styles.photo_list}>
      {photos.map((photo) => (
        <div key={photo.id} className={styles.photo_item}>
          <img src={photo.url} alt={photo.title} />
          <p>{photo.title}</p>
        </div>
      ))}
      {children}
    </div>
  );
};

Photos.propTypes = {
    photos: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
      })
    ).isRequired,
    children: PropTypes.node
  };

export default Photos;
